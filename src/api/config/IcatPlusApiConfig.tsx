import { Context, createContext, useContext } from 'react';

export type IcatPlusApiConfig = {
  // Base URL of the ICAT+ API
  baseUrl: string;

  onError?(string: string): void;

  sessionId?: string;

  sessionIdExpirationTime?: string;

  onExpiredSessionId?(): void;
};

export const IcatPlusAPIContext = setupAPIContext();

function setupAPIContext(): Context<IcatPlusApiConfig | undefined> {
  //this is to ensure that the context is only created once and that it is shared between different instances of the library
  if (!(window as any).IcatPlusAPIContext) {
    (window as any).IcatPlusAPIContext = createContext<
      IcatPlusApiConfig | undefined
    >(undefined);
  }
  return (window as any).IcatPlusAPIContext;
}

export function useIcatPlusApiConfig(): IcatPlusApiConfig {
  const value = useContext(IcatPlusAPIContext);

  if (!value) {
    throw new Error(
      'To use the icat plus API you need to define the configuration using IcatPlusAPIContext',
    );
  }

  return value;
}
