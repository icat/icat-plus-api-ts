import { replaceEqualDeep, type UseQueryOptions } from '@tanstack/react-query';
import type { IcatPlusApiConfig } from '../config';
import type { EndpointDefinition, GetEndpointArgs } from '../endpoints';
import { buildURL } from './buildUrl';
import { fetchEndpoint } from './fetchEndpoint';

export function getQueryObject<DATA, PARAMS>(args: {
  endpoint: EndpointDefinition<
    DATA,
    PARAMS,
    'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE'
  >;
  params?: PARAMS;
  config: IcatPlusApiConfig;
  body?: any;
  skipFetch?: boolean;
  onFail?: (body: string) => void;
  progressReporter?: (progress: number) => void;
}) {
  if (args.endpoint.authenticated && !args.config.sessionId)
    console.error('Endpoint is authenticated but no sessionId was provided');

  const url = buildURL(args.endpoint, args.params, args.config);

  const key = [
    args.endpoint.name,
    args.params,
    args.body,
    `token=${args.config.sessionId}&exptime=${args.config.sessionIdExpirationTime}`, // this will trigger a refetch if the user session changes or is refreshed
    args.config.sessionId || 'unknown',
    url,
    args.skipFetch ? 'skip' : 'fetch',
  ];

  return {
    queryKey: key,
    queryFn: () => fetchEndpoint({ ...args, url, key }),
  };
}

const DEFAULT_STALE_TIME_MINUTES = 20;
const DEFAULT_AUTO_REFRESH_INTERVAL_MINUTES = 5;

export function getQueryOptions<DATA, PARAMS, DEFAULT>(
  args: GetEndpointArgs<DATA, any, DEFAULT>,
) {
  const { endpoint, options } = args;

  // data will be automatically re-fetched if endpoint is IcatPlusApiConfigured to auto-refresh or if this specific call was set to do so
  const shouldRefresh =
    endpoint.autoRefresh || options?.refetchIntervalMinutes !== undefined;

  // data will be automatically re-fetched at this frequency if set, without any user action
  const refreshTimeMinutes = shouldRefresh
    ? options?.refetchIntervalMinutes ||
      (endpoint.autoRefresh && endpoint.autoRefreshIntervalMinutes) ||
      DEFAULT_AUTO_REFRESH_INTERVAL_MINUTES
    : null;

  // data will be considered stale after this time, and will be re-fetched if the user requests it
  // (by navigating to a component that requests it, or by giving focus back to the tab)
  const staleTimeMinutes =
    typeof refreshTimeMinutes === 'number'
      ? refreshTimeMinutes
      : DEFAULT_STALE_TIME_MINUTES;

  // Some hints to understand this:
  // https://tanstack.com/query/latest/docs/framework/react/guides/important-defaults
  // https://tanstack.com/query/latest/docs/framework/react/reference/useQuery
  const res: Partial<
    UseQueryOptions<DATA | null, any, DATA, (string | PARAMS | undefined)[]>
  > = {
    notifyOnChangeProps: ['data'],
    structuralSharing: (oldData, newData) => {
      return replaceEqualDeep(oldData, newData);
    },
    refetchInterval: refreshTimeMinutes
      ? typeof refreshTimeMinutes === 'function'
        ? (query) =>
            refreshTimeMinutes(
              query.state.data as DATA | undefined,
              query.queryKey[1] as PARAMS | undefined,
            ) *
            60 *
            1000
        : refreshTimeMinutes * 60 * 1000
      : undefined,
    refetchIntervalInBackground: false,
    staleTime: staleTimeMinutes * 60 * 1000,
    refetchOnMount: true,
    refetchOnReconnect: true,
    refetchOnWindowFocus: true,
    retry: false,
    gcTime: 30 * 60 * 1000,
  };

  return res;
}
