import { createContext, useContext, useMemo } from 'react';

export type ProgressContextType = {
  progress: number | undefined;
  setProgress: (progress: number) => void;
};

export const ProgressContext = createContext<ProgressContextType>({
  progress: undefined,
  setProgress: () => {},
});

export function useProgressReporter() {
  const { setProgress } = useContext(ProgressContext);

  return useMemo(() => setProgress, [setProgress]);
}

export function readResponseWithProgress(
  response: Response | null,
  progressReporter?: (progress: number) => void,
): Response | null {
  if (!response) return response;

  const reader = response?.body?.getReader();
  if (!reader) throw new Error('No reader');
  const contentLength = Number(response?.headers.get('content-length'));
  let loaded = 0;
  let progress = 0;
  const stream = new ReadableStream({
    start(controller) {
      if (!reader) return;
      function push() {
        reader?.read().then(({ done, value }) => {
          if (done) {
            controller.close();
            if (progressReporter) progressReporter(1);
            return;
          }
          loaded += value?.length || 0;
          if (progressReporter && contentLength) {
            const newProgress = loaded / contentLength;
            if (newProgress - progress > 0.01) {
              progress = newProgress;
              progressReporter(progress);
            }
          }
          controller.enqueue(value);
          push();
        });
      }
      push();
    },
  });

  return new Response(stream, {
    headers: response.headers,
    status: response.status,
    statusText: response.statusText,
  });
}
