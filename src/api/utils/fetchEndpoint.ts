import type { IcatPlusApiConfig } from '../config';
import type { EndpointDefinition } from '../endpoints';
import { readResponseWithProgress } from './progress';

export function fetchEndpoint<DATA, PARAMS>(args: {
  endpoint: EndpointDefinition<
    DATA,
    PARAMS,
    'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE'
  >;
  params?: PARAMS;
  config: IcatPlusApiConfig;
  body?: any;
  url: string;
  key?: any;
  isFormData?: boolean;
  skipFetch?: boolean;
  onFail?: (body: string) => void;
  progressReporter?: (progress: number) => void;
}) {
  if (args.skipFetch) return Promise.resolve(null);

  return fetch(args.url, {
    method: args.endpoint.method,
    body: args.body
      ? args.isFormData
        ? args.body
        : JSON.stringify(args.body)
      : undefined,
    ...(args.isFormData
      ? {}
      : {
          headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
          },
        }),
  })
    .catch((error) => {
      console.error(error);
      return null;
    })
    .then((response) =>
      readResponseWithProgress(response, args.progressReporter),
    )
    .then(async (response) => {
      const body = await response?.text();

      if (body?.includes('Session is not found for this sessionId.')) {
        args.config.onExpiredSessionId && args.config.onExpiredSessionId();
        return null;
      }
      if (response?.status && [401, 403, 404].includes(response?.status)) {
        // unauthorized, forbidden or not found are expected and should not trigger a an error, ony an empty result
        return null;
      }
      if (!response?.ok) {
        console.error(
          `Error fetching ${args.endpoint.method} ${args.url}: ${response?.status} ${response?.statusText} ${body}`,
        );
        if (args.onFail && body) args.onFail(body);
        return null;
      }
      if (!body) return null;
      try {
        return JSON.parse(body) as DATA;
      } catch (e) {
        return body as DATA;
      }
    });
}
