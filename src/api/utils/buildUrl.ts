import type { IcatPlusApiConfig } from '../config';
import type { EndpointDefinition } from '../endpoints';

export function buildURL(
  endpoint: EndpointDefinition<any, any, any>,
  params: any,
  config: IcatPlusApiConfig,
) {
  const serverUrl = config.baseUrl;
  const url = `${serverUrl}${endpoint.module}${
    endpoint.authenticated ? '/' + config.sessionId : ''
  }${endpoint.path}`;

  if (!params) return url;

  const urlWithPathParams = Object.keys(params).reduce(
    (a, b) => a.replaceAll(`:${b}`, params[b]),
    url,
  );

  const queryParams = Object.keys(params).filter((key) => {
    return !url.includes(`:${key}`);
  });

  if (queryParams.length === 0) return urlWithPathParams;

  const queryParamsObject = new URLSearchParams();
  queryParams.forEach((key) => {
    queryParamsObject.append(key, params[key]);
  });

  return `${urlWithPathParams}?${queryParamsObject.toString()}`;
}
