import { useQueryClient } from '@tanstack/react-query';
import { useCallback } from 'react';
import { readResponseWithProgress } from '../utils/progress';

/**
 * This allows to fetch a base64 encoded image from a url and cache it in the react-query cache
 * @returns
 */
export function useFetchBase64() {
  const queryClient = useQueryClient();
  return useCallback(
    (src: string, onProgress?: (v: number) => void) =>
      queryClient.fetchQuery({
        staleTime: Infinity,
        queryKey: ['base64', src],
        queryFn: () => {
          return fetch(src)
            .then((res) => {
              return readResponseWithProgress(res, onProgress);
            })
            .then((res) => res?.blob())
            .then((blob) => {
              return new Promise<string | undefined>((resolve, reject) => {
                if (!blob) return resolve(undefined);
                // read the blob as a data URL
                const reader = new FileReader();
                reader.onloadend = () =>
                  resolve(reader.result?.toString() || undefined);
                reader.onerror = reject;
                reader.readAsDataURL(blob);
              });
            });
        },
      }),
    [queryClient],
  );
}
