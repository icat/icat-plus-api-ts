import { useCallback } from 'react';
import { useIcatPlusApiConfig } from '../config';

export function useNotifyOfError() {
  const config = useIcatPlusApiConfig();

  return useCallback(
    (body: string) => {
      if (config.onError) {
        try {
          const message = JSON.parse(body).message;
          config.onError(message || body);
        } catch (e) {
          config.onError(body);
        }
      }
    },
    [config],
  );
}
