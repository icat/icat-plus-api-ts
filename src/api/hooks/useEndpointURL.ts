import { useIcatPlusApiConfig } from '../../api/config/IcatPlusApiConfig';
import type { EndpointDefinition } from '../endpoints';
import { buildURL } from '../utils/buildUrl';

export function useEndpointURL<DATA, PARAMS>(
  endpoint: EndpointDefinition<DATA, PARAMS, 'GET'>,
): (params?: PARAMS) => string {
  const config = useIcatPlusApiConfig();

  return (params?: PARAMS) => {
    return buildURL(endpoint, params, config);
  };
}
