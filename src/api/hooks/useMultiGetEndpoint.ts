import { useSuspenseQueries } from '@tanstack/react-query';
import { useMemo } from 'react';
import { useIcatPlusApiConfig } from '../config';
import type { GetEndpointArgs } from '../endpoints';
import { getQueryOptions, getQueryObject } from '../utils/query';
import { useNotifyOfError } from './useNotifyOfError';

export type GetEndpointsArgs<DATA, PARAMS, DEFAULT> = Omit<
  GetEndpointArgs<DATA, PARAMS, DEFAULT>,
  'params'
> & {
  params?: PARAMS[];
};
/**
 * Allows to call the same endpoint a variable number of times with different parameters
 * @param args.endpoint Definition of the endpoint to be called
 * @param args.params List of parameters to be passed to each call of the endpoint, as path param or query param depending on the endpoint definition
 * @param args.options Override options for the react-query hook
 * @param args.default Default value to be returned if fetch fails
 * @param args.skipFetch If true, the fetch will not be executed and the default value will be returned
 */
export function useMultiGetEndpoint<DATA, PARAMS, DEFAULT = undefined>(
  args: GetEndpointsArgs<DATA, PARAMS, DEFAULT>,
): (DATA | DEFAULT)[] {
  const config = useIcatPlusApiConfig();

  const notifyOfError = useNotifyOfError();

  const queries = useMemo(
    () => ({
      queries:
        args.params?.map((param) => {
          return {
            ...getQueryOptions(args),
            ...getQueryObject({
              endpoint: args.endpoint,
              params: param,
              config,
              skipFetch: args.skipFetch,
              onFail: args.notifyOnError === false ? undefined : notifyOfError,
            }),
          };
        }) || [],
    }),
    [args, config, notifyOfError],
  );

  const queriesResult = useSuspenseQueries(queries);

  const res = useMemo(
    () =>
      queriesResult.map((queryResult) =>
        queryResult.data
          ? (JSON.parse(JSON.stringify(queryResult.data)) as DATA) //make a copy so that the data cannot be mutated
          : (args.default as DEFAULT),
      ),
    [queriesResult], // eslint-disable-line react-hooks/exhaustive-deps
  );
  return res;
}
