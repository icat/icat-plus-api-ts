import { useQueryClient } from '@tanstack/react-query';
import { useIcatPlusApiConfig } from '../config';
import type { EndpointDefinition } from '../endpoints';
import { getQueryObject } from '../utils/query';

export function useAsyncFetchEndpoint<S, P>(
  endpoint: EndpointDefinition<S, P, 'GET'>,
) {
  const queryClient = useQueryClient();
  const config = useIcatPlusApiConfig();

  return (params?: P) => {
    const query = getQueryObject({
      endpoint,
      params,
      config,
      skipFetch: false,
    });

    return queryClient.fetchQuery(query);
  };
}
