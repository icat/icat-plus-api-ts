import { useSuspenseQuery } from '@tanstack/react-query';
import { useMemo } from 'react';
import { useIcatPlusApiConfig } from '../config';
import type { GetEndpointArgs } from '../endpoints';
import { useProgressReporter } from '../utils/progress';
import { getQueryObject, getQueryOptions } from '../utils/query';
import { useNotifyOfError } from './useNotifyOfError';

/**
 * Allows to call an endpoint and get the result
 * @param args.endpoint Definition of the endpoint to be called
 * @param args.params Parameters to be passed to the endpoint, as path param or query param depending on the endpoint definition
 * @param args.options Override options for the react-query hook
 * @param args.default Default value to be returned if fetch fails
 * @param args.skipFetch If true, the fetch will not be executed and the default value will be returned
 * @returns Returns the data returned by the fetch or the default value if the fetch fails
 */
export function useGetEndpoint<DATA, PARAMS, DEFAULT = undefined>(
  args: GetEndpointArgs<DATA, PARAMS, DEFAULT>,
): DATA | DEFAULT {
  const config = useIcatPlusApiConfig();

  const notifyOfError = useNotifyOfError();

  const progressReporter = useProgressReporter();

  const query = useMemo(
    () => ({
      ...getQueryObject({
        ...args,
        config,
        onFail: args.notifyOnError === false ? undefined : notifyOfError,
        progressReporter,
      }),
      ...getQueryOptions(args),
    }),
    [args, config, notifyOfError, progressReporter],
  );

  const queryResult = useSuspenseQuery(query);

  const res = useMemo(
    () =>
      queryResult.data
        ? (JSON.parse(JSON.stringify(queryResult.data)) as DATA) //make a copy so that the data cannot be mutated
        : (args.default as DEFAULT),
    [queryResult.data], // eslint-disable-line react-hooks/exhaustive-deps
  );

  return res;
}
