import {
  type UseMutationOptions,
  useQueryClient,
  useMutation,
  UseMutationResult,
} from '@tanstack/react-query';
import { useIcatPlusApiConfig } from '../config';
import type { EndpointDefinition } from '../endpoints';
import { buildURL } from '../utils/buildUrl';
import { fetchEndpoint } from '../utils/fetchEndpoint';
import { useNotifyOfError } from './useNotifyOfError';

export type MutateEndpointArgs<DATA, PARAMS> = {
  endpoint: EndpointDefinition<
    DATA,
    PARAMS,
    'PUT' | 'POST' | 'DELETE' | 'PATCH'
  >;
  params?: PARAMS;
  options?: {
    refetchIntervalMinutes?: number; //in minutes
  };
} & UseMutationOptions<any, any, any, any>;

/**
 * @param args.endpoint Definition of the endpoint to be called
 * @param args.params Parameters to be passed to the endpoint, as path param or query param depending on the endpoint definition
 * @param args.options Override options for the react-query hook
 * @returns Returns the data returned by the fetch or undefined if the fetch fails
 */
export function useMutateEndpoint<DATA, PARAMS>(
  args: MutateEndpointArgs<DATA, PARAMS>,
): UseMutationResult<
  DATA | undefined,
  any,
  {
    body: any;
    isFormData?: boolean | undefined;
    params?: PARAMS | undefined;
  },
  any
> {
  const queryClient = useQueryClient();
  const config = useIcatPlusApiConfig();

  const notifyOfError = useNotifyOfError();

  return useMutation<
    DATA | undefined,
    any,
    {
      body: any;
      isFormData?: boolean;
      params?: PARAMS;
    },
    any
  >({
    mutationFn: (mutationParams) => {
      const url = buildURL(
        args.endpoint,
        {
          ...(args.params ? args.params : {}),
          ...(mutationParams.params ? mutationParams.params : {}),
        },
        config,
      );
      return fetchEndpoint<DATA, PARAMS>({
        ...args,
        body: mutationParams.body,
        url,
        isFormData: mutationParams.isFormData,
        onFail: notifyOfError,
        config: config,
      })
        .then((e) => {
          queryClient.invalidateQueries({
            queryKey: [args.endpoint.name],
          });
          return e ? e : undefined;
        })
        .catch((e) => {
          return undefined;
        });
    },
    ...args,
  });
}
