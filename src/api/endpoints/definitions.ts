export type EndpointDefinitionParams<
  DATA, // eslint-disable-line @typescript-eslint/no-unused-vars
  PARAMS, // eslint-disable-line @typescript-eslint/no-unused-vars
  METHOD extends 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE',
> = {
  name: string;
  module: string;
  path?: string;
  method: METHOD;
  authenticated?: boolean;
} & (
  | {
      autoRefresh: true;
      autoRefreshIntervalMinutes:
        | number
        | ((data: DATA | undefined, params: PARAMS | undefined) => number);
    }
  | {
      autoRefresh: false;
    }
);

export type EndpointDefinition<
  DATA,
  PARAMS,
  METHOD extends 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE',
> = Required<EndpointDefinitionParams<DATA, PARAMS, METHOD>>;

//encapsulate the endpoint definition in a function to help with type inference and provide default values for optional parameters
export function IcatPlusEndpoint<
  DATA,
  PARAMS,
  METHOD extends 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE',
>(
  args: EndpointDefinitionParams<DATA, PARAMS, METHOD> & {
    schema?: DATA;
    params?: PARAMS;
  },
): EndpointDefinition<DATA, PARAMS, METHOD> {
  return {
    authenticated: true,
    path: '',
    ...args,
  };
}

export type GetEndpointArgs<DATA, PARAMS, DEFAULT> = {
  endpoint: EndpointDefinition<DATA, PARAMS, 'GET'>;
  params?: PARAMS;
  options?: {
    refetchIntervalMinutes?: number; //in minutes
  };
  default?: DEFAULT;
  skipFetch?: boolean;
  notifyOnError?: boolean;
};
