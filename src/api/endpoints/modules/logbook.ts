import type {
  LogbookEvent,
  LogbookInvestigationCount,
  LogbookInvestigationStat,
  LogbookTag,
} from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export enum DownloadType {
  json = 'json',
  pdf = 'pdf',
  txt = 'txt',
}

export type EventListParams = {
  investigationId?: string;
  instrumentName?: string;
  sortBy?: string;
  sortOrder?: -1 | 1;
  skip?: number;
  limit?: number;
  types?: string;
  filterInvestigation?: boolean;
  search?: string;
  format?: DownloadType;
  date?: string;
  tags?: string;
  startTime?: string;
  endTime?: string;
};
export const LOGBOOK_EVENT_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/event',
  name: 'logbookevent',
  method: 'GET',
  schema: {} as LogbookEvent[],
  params: {} as EventListParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 1,
});

export const LOGBOOK_EVENT_COUNT_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/event/count',
  name: 'logbookevent',
  method: 'GET',
  schema: {} as { totalNumber: number }[],
  params: {} as EventListParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 1,
});

export const LOGBOOK_TAG_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/tag',
  name: 'logbooktag',
  method: 'GET',
  schema: {} as LogbookTag[],
  params: {} as {
    instrumentName?: string;
    investigationId?: string;
  },
  autoRefresh: true,
  autoRefreshIntervalMinutes: 1,
});

export const LOGBOOK_UPDATE_TAG_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/tag',
  name: 'logbooktag',
  method: 'PUT',
  schema: {} as LogbookTag,
  params: {} as {
    id?: string;
    investigationId?: number;
    instrumentName?: string;
  },
  autoRefresh: false,
});

export const LOGBOOK_CREATE_TAG_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/tag',
  name: 'logbooktag',
  method: 'POST',
  schema: {} as LogbookTag,
  params: {} as {
    investigationId?: number;
    instrumentName?: string;
  },
  autoRefresh: false,
});

export const LOGBOOK_EVENT_UPDATE_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/event',
  name: 'logbookevent',
  method: 'PUT',
  schema: {} as LogbookEvent,
  params: {} as {
    investigationId?: number;
    instrumentName?: string;
  },
  autoRefresh: false,
});

export const LOGBOOK_EVENT_CREATE_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/event',
  name: 'logbookevent',
  method: 'POST',
  schema: {} as LogbookEvent,
  params: {} as {
    investigationId?: number;
    instrumentName?: string;
  },
  autoRefresh: false,
});

export const LOGBOOK_EVENT_CREATE_BASE64_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/event/createfrombase64',
  name: 'logbookevent',
  method: 'POST',
  schema: undefined,
  params: {} as {
    investigationId?: number;
    instrumentName?: string;
  },
  autoRefresh: false,
});

export const LOGBOOK_EVENT_DATES_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/event/dates',
  name: 'logbookevent',
  method: 'GET',
  schema: {} as {
    _id: string;
    event_count: number;
  }[],
  params: {} as EventListParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 10,
});

export const LOGBOOK_EVENT_PAGE_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  path: '/event/page',
  name: 'logbookevent',
  method: 'GET',
  schema: {} as {
    page: { total: number; limit: number; index: number };
  },
  params: {} as {
    _id: string;
    limit: number;
    investigationId?: string;
    instrumentName?: string;
    sortBy?: string;
    sortOrder?: -1 | 1;
    types?: string;
    filterInvestigation?: boolean;
    search?: string;
    date?: string;
    startTime?: string;
    endTime?: string;
  },
  autoRefresh: false,
});

export const LOGBOOK_INVESTIGATION_STATISTICS_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  name: 'logbook_investigation_statistics',
  method: 'GET',
  schema: [] as LogbookInvestigationStat[],
  path: '/stats/investigation',
  params: {} as {
    startDate: string;
    endDate: string;
  },
  autoRefresh: true,
  autoRefreshIntervalMinutes: 10,
});

export const LOGBOOK_COUNT_STATISTICS_ENDPOINT = IcatPlusEndpoint({
  module: 'logbook',
  name: 'logbook_count_statistics',
  method: 'GET',
  schema: [] as LogbookInvestigationCount[],
  path: '/stats/count',
  params: {} as {
    startDate: string;
    endDate: string;
    type: 'annotation' | 'notification';
  },
  autoRefresh: true,
  autoRefreshIntervalMinutes: 10,
});
