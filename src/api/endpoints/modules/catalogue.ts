import { add, parse } from 'date-fns';
import type {
  Investigation,
  DatasetTimeLine,
  Dataset,
  Sample,
  InvestigationUser,
  Instrument,
  DatafileResponse,
  DatasetParameterSearchResult,
  Parameter,
  InstrumentScientist,
  SimpleIcatUser,
  DataCollectionType,
  DataCollection,
  ListMeta,
  Technique,
  SampleInformation,
} from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export const ICAT_DATE_FORMAT = 'yyyy-MM-dd';

function isReleasedInvestigation(investigation?: Investigation) {
  if (!investigation) return false;
  if (!investigation.doi) return false;

  if (!investigation.releaseDate) return false;

  const releaseDate = parse(
    investigation.releaseDate,
    ICAT_DATE_FORMAT,
    new Date(),
  );
  if (!releaseDate) {
    return false;
  }
  return releaseDate.getTime() < Date.now();
}

function isFinishedInvestigation(investigation?: Investigation) {
  if (!investigation) return false;

  if (!investigation.endDate) return false;
  const endDate = parse(investigation.endDate, ICAT_DATE_FORMAT, new Date());
  if (!endDate) {
    return false;
  }
  // investigation is finished for sure 48 hours after the end date
  const finishedDate = add(endDate, { hours: 48 });
  if (!finishedDate) {
    return false;
  }
  return finishedDate.getTime() <= Date.now();
}

function getRefreshTimeForInvestigation<
  DATA extends { investigation?: Investigation },
>(data: DATA[] | undefined, investigationId?: string | number) {
  if (investigationId) {
    const investigations = data?.map((dataset) => dataset.investigation);
    if (investigations?.length) {
      const investigation = investigations[0];
      if (
        isReleasedInvestigation(investigation) ||
        isFinishedInvestigation(investigation)
      ) {
        return 20;
      }
    }
  }
  return 1;
}

function computeRefreshTimeForInvestigation<
  DATA extends { investigation?: Investigation },
  PARAM extends {
    investigationId?: string | number;
  },
>(data: DATA[] | undefined, params: PARAM | undefined) {
  return getRefreshTimeForInvestigation(data, params?.investigationId);
}

function computeRefreshTimeForInvestigations<
  DATA extends { investigation?: Investigation },
  PARAM extends {
    investigationIds?: string | number;
  },
>(data: DATA[] | undefined, params: PARAM | undefined) {
  return getRefreshTimeForInvestigation(data, params?.investigationIds);
}

export type Role =
  | 'participant'
  | 'instrumentscientist'
  | 'released'
  | 'embargoed'
  | 'industry';

export type InvestigationListParams = {
  ids?: string;
  instrumentName?: string;
  filter?: Role;
  startDate?: string;
  endDate?: string;
  limit?: number;
  skip?: number;
  search?: string;
  sortBy?:
    | 'NAME'
    | 'INSTRUMENT'
    | 'STARTDATE'
    | 'TITLE'
    | 'SUMMARY'
    | 'RELEASEDATE'
    | 'DOI';
  sortOrder?: -1 | 1;
  investigationName?: string;
  time?: string;
  investigationId?: string;
  withHasAccess?: boolean;
  username?: string;
  parameters?: string;
};

export const INVESTIGATION_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/investigation',
  name: 'investigation',
  method: 'GET',
  schema: {} as Investigation[],
  params: {} as InvestigationListParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 20,
});

export type InvestigationParams = {
  investigationId: string;
};

export type DatasetTimelineParams = {
  investigationId?: string;
  datasetIds?: string;
  limit?: number;
  skip?: number;
  search?: string;
  datasetType?: string;
  sortBy?: 'NAME' | 'STARTDATE' | 'SAMPLENAME';
  sortOrder?: -1 | 1;
  nested?: boolean;
  sampleId?: string | number;
  parameters?: string;
  instrumentName?: string;
};

export const DATASET_TIMELINE_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/dataset/timeline',
  name: 'dataset',
  method: 'GET',
  schema: {} as DatasetTimeLine[],
  params: {} as DatasetTimelineParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 1,
});

export type DatasetParams = {
  investigationIds?: string;
  datasetIds?: string;
  limit?: number;
  skip?: number;
  search?: string;
  datasetType?: string;
  sortBy?: 'NAME' | 'STARTDATE' | 'SAMPLENAME';
  sortOrder?: -1 | 1;
  nested?: boolean;
  sampleId?: string | number;
  parameters?: string;
  instrumentName?: string;
};

export const DATASET_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/dataset',
  name: 'dataset',
  method: 'GET',
  schema: {} as Dataset[],
  params: {} as DatasetParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: computeRefreshTimeForInvestigations,
});

export type SampleListParams = {
  investigationId?: number;
  instrumentName?: string;
  limit?: number;
  skip?: number;
  search?: string;
  sampleIds?: string;
  startDate?: string;
  endDate?: string;
  datasetType?: string;
  havingAcquisitionDatasets?: boolean;
  includeDatasets?: boolean;
  nested?: boolean;
  sortBy?: 'NAME' | 'DATE';
  sortOrder?: -1 | 1;
  parameters?: string;
};

export const SAMPLE_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/samples',
  name: 'sample',
  method: 'GET',
  schema: {} as Sample[],
  params: {} as SampleListParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: computeRefreshTimeForInvestigation,
});

export interface SamplePageParams
  extends Omit<SampleListParams, 'includeDatasets' | 'nested' | 'datasetType'> {
  sampleId: string;
}

export const SAMPLE_PAGE_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/samples/:sampleId/page',
  name: 'sample',
  method: 'GET',
  schema: {} as ListMeta,
  params: {} as SamplePageParams,
  autoRefresh: false,
});

export type InvestigationUsersParams = {
  investigationId: string;
};
export const INVESTIGATION_USERS_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/investigation/id/:investigationId/investigationusers',
  name: 'investigationusers',
  method: 'GET',
  schema: {} as InvestigationUser[],
  params: {} as InvestigationUsersParams,
  autoRefresh: false,
});

export const INVESTIGATION_USERS_ADD_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/investigation/id/:investigationId/investigationusers',
  name: 'investigationusers',
  method: 'POST',
  schema: {} as InvestigationUser,
  params: {} as InvestigationUsersParams,
  autoRefresh: false,
});

export const INVESTIGATION_USERS_DELETE_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/investigation/id/:investigationId/investigationusers',
  name: 'investigationusers',
  method: 'DELETE',
  schema: {} as InvestigationUser[],
  params: {} as InvestigationUsersParams,
  autoRefresh: false,
});

export type InstrumentListParams = {
  filter?: string;
};
export const USER_INSTRUMENT_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/instruments',
  name: 'instrument',
  method: 'GET',
  schema: {} as Instrument[],
  params: {} as InstrumentListParams,
  autoRefresh: false,
});

export const INSTRUMENT_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/instruments',
  name: 'instrument',
  method: 'GET',
  authenticated: false,
  schema: {} as Instrument[],
  params: undefined,
  autoRefresh: false,
});
export type DatasetFileListParams = {
  datasetId: string;
  limit?: number;
  skip?: number;
  search?: string;
};
export const DATASET_FILE_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/datafile',
  name: 'dataset',
  method: 'GET',
  schema: {} as DatafileResponse[],
  params: {} as DatasetFileListParams,
  autoRefresh: false,
});

export type DatasetParametersParams = {
  investigationId?: string;
  name?: string;
  datasetType?: 'acquisition' | 'processed';
  sampleId?: string;
  instrumentName?: string;
  startDate?: string;
  endDate?: string;
  unique?: boolean;
  parameters?: string;
};
export const DATASET_PARAMETERS_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/datasetparameter',
  name: 'dataset',
  method: 'GET',
  schema: {} as DatasetParameterSearchResult[],
  params: {} as DatasetParametersParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 1,
});

export type DatasetParameterValuesResult = {
  name: string;
  values: string[];
};

export const DATASET_PARAMETER_VALUE_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/datasetparameter/values',
  name: 'dataset',
  method: 'GET',
  schema: [] as DatasetParameterValuesResult[],
  params: {} as Omit<DatasetParametersParams, 'unique'>,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 1,
});

export type SampleParametersParams = {
  sampleId: string;
  investigationId: string;
};
export const CREATE_SAMPLE_PARAMETER = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/samples/:sampleId/sampleParameters',
  name: 'sample',
  method: 'POST',
  schema: {} as string,
  params: {} as SampleParametersParams,
  autoRefresh: false,
});

export type ParametersParams = {
  id?: string;
  investigationId: string;
};
export const UPDATE_SAMPLE_PARAMETER = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/sampleParameters/:id',
  name: 'sample',
  method: 'PUT',
  schema: {} as Parameter,
  params: {} as ParametersParams,
  autoRefresh: false,
});

export const GET_INSTRUMENT_SCIENTIST_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/instrumentscientist',
  name: 'instrumentscientist',
  method: 'GET',
  schema: {} as InstrumentScientist[],
  params: {} as {
    instrumentName?: string;
  },
  autoRefresh: false,
});

export const ADD_INSTRUMENT_SCIENTIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/instrumentscientist',
  name: 'instrumentscientist',
  method: 'PUT',
  schema: {} as InstrumentScientist[],
  params: {} as {
    instrumentnames: string;
    username: string;
  },
  autoRefresh: false,
});

export const DELETE_INSTRUMENT_SCIENTIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/instrumentscientist',
  name: 'instrumentscientist',
  method: 'DELETE',
  schema: undefined,
  params: {} as {
    instrumentscientistsid: number[];
  },
  autoRefresh: false,
});

export const USER_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/user',
  name: 'user',
  method: 'GET',
  schema: {} as SimpleIcatUser[],
  params: undefined,
  autoRefresh: false,
});

export const USER_LIST_PRINCIPAL_INVESTIGATOR_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/investigation/:investigationId/user',
  name: 'user',
  method: 'GET',
  schema: {} as SimpleIcatUser[],
  params: {} as {
    investigationId: number;
  },
  autoRefresh: false,
});

export type DataCollectionListParams = {
  datasetId?: string;
  type?: DataCollectionType;
  skip?: number;
  limit?: number;
  search?: string;
  sortBy?: 'DOI' | 'DATE';
  sortOrder?: -1 | 1;
};
export const DATA_COLLECTION_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/datacollection',
  name: 'datacollection',
  method: 'GET',
  schema: {} as DataCollection[],
  params: {} as DataCollectionListParams,
  autoRefresh: false,
});

export const TECHNIQUE_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/techniques',
  name: 'technique',
  method: 'GET',
  authenticated: false,
  schema: {} as Technique[],
  params: undefined,
  autoRefresh: false,
});

export type FileListParams = {
  sampleId: number;
  groupName?: string;
};

export const SAMPLE_FILES_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/files',
  name: 'files',
  method: 'GET',
  schema: {} as SampleInformation,
  params: {} as FileListParams,
  autoRefresh: false,
});

export const SAMPLE_UPLOAD_FILE_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/files/upload',
  name: 'files',
  method: 'POST',
  schema: {} as SampleInformation,
  params: {} as {
    sampleId: number;
  },
  autoRefresh: false,
});

export const SAMPLE_DOWNLOAD_FILE_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/files/download',
  name: 'files',
  method: 'GET',
  schema: {} as never,
  params: {} as {
    sampleId: number;
    resourceId: number;
  },
  autoRefresh: false,
});

export const PARAMETERS_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'catalogue',
  path: '/parameters',
  name: 'parameter',
  method: 'GET',
  authenticated: false,
  schema: {} as Parameter[],
  params: {} as { parameterTypeId?: number; name?: string },
  autoRefresh: false,
});
