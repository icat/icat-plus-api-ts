import { IcatPlusEndpoint } from '../definitions';

export type DmpUuid = {
  uuid: string;
};
export const DMP_QUESTIONNAIRES_ENDPOINT = IcatPlusEndpoint({
  module: 'dmp',
  path: '/questionnaires',
  name: 'questionnaires',
  method: 'GET',
  schema: {} as DmpUuid[],
  params: {} as { investigationId: string; investigationName: string },
  autoRefresh: false,
});
