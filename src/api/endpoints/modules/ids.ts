import type { DatasetPaths, DatasetStatus } from '../../models/ids';
import { IcatPlusEndpoint } from '../definitions';

export type DatasetIdsParams = {
  datasetIds: string;
};
export const DATASET_STATUS_ENDPOINT = IcatPlusEndpoint({
  module: 'ids',
  path: '/datasets/status',
  name: 'datasetStatus',
  method: 'GET',
  schema: {} as DatasetStatus[],
  params: {} as DatasetIdsParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 20,
});

export type DatafileDownloadParams = {
  datafileIds?: string;
  datasetIds?: string;
};
export const DATA_DOWNLOAD_ENDPOINT = IcatPlusEndpoint({
  module: 'ids',
  path: '/data/download',
  name: 'datafile',
  method: 'GET',
  schema: '',
  params: {} as DatafileDownloadParams,
  autoRefresh: false,
});

export const DATA_RESTORE_ENDPOINT = IcatPlusEndpoint({
  module: 'ids',
  path: '/datasets/restore',
  name: 'datasetsRestore',
  method: 'POST',
  schema: '',
  params: {} as DatasetIdsParams,
  autoRefresh: false,
});

export type PathIdsParams = {
  datafileIds?: string;
  datasetIds?: string;
  investigationId: number;
  isonline?: boolean;
};

export const GET_PATH_ENDPOINT = IcatPlusEndpoint({
  module: 'ids',
  path: '/path',
  name: 'path',
  method: 'GET',
  schema: {} as DatasetPaths,
  params: {} as PathIdsParams,
  autoRefresh: false,
});
