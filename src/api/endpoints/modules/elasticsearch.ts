import { IcatPlusEndpoint } from '../definitions';

export const ELASTICSEARCH_METRICS_ENDPOINT = IcatPlusEndpoint({
  module: 'elasticsearch',
  name: 'elasticsearch_metrics',
  method: 'POST',
  schema: {} as any,
  path: '/stats/instrument/metrics',

  params: {} as {
    instrumentName?: string;
    ranges?: {
      start: string;
      end: string;
      name: string;
    }[];
  },
  autoRefresh: true,
  autoRefreshIntervalMinutes: 10,
});

export const ELASTICSEARCH_HISTOGRAM_ENDPOINT = IcatPlusEndpoint({
  module: 'elasticsearch',
  name: 'elasticsearch_histogram',
  method: 'GET',
  schema: {} as any,
  path: '/stats/histogram',
  params: {} as {
    instrumentName?: string;
    startDate: string;
    endDate: string;
    format: 'csv' | 'json' | 'csv_json';
    metric?: 'fileCount' | 'volume';
    aggregation: 'instrumentName' | 'definition';
    interval: string;
  },
  autoRefresh: true,
  autoRefreshIntervalMinutes: 10,
});
