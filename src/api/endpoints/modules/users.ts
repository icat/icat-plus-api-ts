import type { UserPreferences } from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export const GET_USER_PREFERENCES_ENDPOINT = IcatPlusEndpoint({
  method: 'GET',
  module: 'users',
  name: 'preferences',
  path: '/preferences',
  schema: {} as UserPreferences,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 5,
});

export const PUT_USER_PREFERENCES_ENDPOINT = IcatPlusEndpoint({
  method: 'PUT',
  module: 'users',
  name: 'preferences',
  path: '/preferences',
  schema: {} as UserPreferences,
  autoRefresh: false,
});
