import { IcatPlusEndpoint } from '../definitions';

export const DOWNLOAD_FILE_ENDPOINT = IcatPlusEndpoint({
  module: 'resource',
  path: '/file/download',
  name: 'download',
  method: 'GET',
  schema: {} as never,
  params: {} as { resourceId: string },
  autoRefresh: false,
});

export const UPLOAD_FILE_ENDPOINT = IcatPlusEndpoint({
  module: 'resource',
  path: '/file/upload',
  name: 'upload',
  method: 'POST',
  schema: {} as { _id: string },
  params: {} as { investigationId?: number; instrumentName?: string },
  autoRefresh: false,
});
