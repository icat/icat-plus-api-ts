import type {
  DataciteDOIAttributes,
  DataCollection,
  Dataset,
  ExpReport,
} from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export type DoiDatasetParams = {
  doiPrefix: string;
  doiSuffix: string;
  limit?: number;
  skip?: number;
  search?: string;
  sortBy?: 'NAME' | 'STARTDATE' | 'SAMPLENAME';
  sortOrder?: -1 | 1;
};

export const DOI_DATASET_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'doi',
  path: '/:doiPrefix/:doiSuffix/datasets',
  name: 'doidatasets',
  method: 'GET',
  schema: {} as Dataset[],
  params: {} as DoiDatasetParams,
  autoRefresh: false,
  authenticated: false,
});

export const MINT_DOI_ENDPOINT = IcatPlusEndpoint({
  module: 'doi',
  path: '/mint',
  name: 'mintdoi',
  method: 'POST',
  schema: {} as {
    message: string;
  },
  params: {} as {
    title: string;
    abstract: string;
    authors: {
      name: string;
      surname: string;
      orcid?: string;
      id: string;
    }[];
    datasetIdList?: number[];
    dataCollectionId?: number;
    keywords?: string[];
    citationPublicationDOI?: string;
    referenceURL?: string;
  },
  autoRefresh: false,
});

export const ICATPLUS_GET_DOI = IcatPlusEndpoint({
  module: 'doi',
  path: '/:doi/json-datacite',
  name: 'datacite',
  method: 'GET',
  schema: {} as DataciteDOIAttributes,
  params: {} as {
    doi: string;
  },
  autoRefresh: false,
  authenticated: false,
});

export const ICATPLUS_GET_EXP_REPORTS = IcatPlusEndpoint({
  module: 'doi',
  path: '/:doi/reports',
  name: 'expreports',
  method: 'GET',
  schema: {} as ExpReport[],
  params: {} as {
    doi: string;
  },
  autoRefresh: false,
  authenticated: false,
});

export const RESERVE_DOI_ENDPOINT = IcatPlusEndpoint({
  module: 'doi',
  path: '/reserve',
  name: 'reservedoi',
  method: 'POST',
  schema: {} as {
    message: string;
  },
  params: {} as {
    datasetIdList: number[];
  },
  autoRefresh: false,
});

export const GET_RESERVED_DOIS_ENDPOINT = IcatPlusEndpoint({
  module: 'doi',
  path: '/reserved-dois',
  name: 'reserveddois',
  method: 'GET',
  schema: {} as DataCollection[],
  autoRefresh: false,
});
