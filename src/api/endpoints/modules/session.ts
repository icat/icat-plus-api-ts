import type { IcatSession, IcatUser } from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export const SESSION_ENDPOINT = IcatPlusEndpoint({
  module: 'session',
  name: 'session',
  method: 'GET',
  schema: {} as IcatSession,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 20,
});

export const SESSION_CREATE_ENDPOINT = IcatPlusEndpoint({
  module: 'session',
  name: 'session_create',
  method: 'POST',
  schema: {} as IcatUser,
  authenticated: false,
  autoRefresh: false,
});

export const SESSION_BY_ID_ENDPOINT = IcatPlusEndpoint({
  module: 'session',
  path: '/:sessionId',
  name: 'session',
  method: 'GET',
  schema: {} as IcatSession,
  params: {} as { sessionId: string },
  authenticated: false,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 20,
});

export const SESSION_REFRESH_ENDPOINT = IcatPlusEndpoint({
  module: 'session',
  path: '/:sessionId',
  name: 'session',
  method: 'PUT',
  schema: undefined,
  params: {} as { sessionId: string },
  authenticated: false,
  autoRefresh: false,
});
