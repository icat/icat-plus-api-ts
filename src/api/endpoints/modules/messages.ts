import type { InformationMessage } from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export const GET_MESSAGES_ENDPOINT = IcatPlusEndpoint({
  method: 'GET',
  module: 'messages',
  name: 'messages',
  authenticated: false,
  schema: {} as InformationMessage[],
  autoRefresh: true,
  autoRefreshIntervalMinutes: 5,
});

export const PUT_INFORMATION_MESSAGES_ENDPOINT = IcatPlusEndpoint({
  method: 'PUT',
  module: 'messages',
  name: 'information',
  path: '/information',
  schema: {} as InformationMessage,
  autoRefresh: false,
});
