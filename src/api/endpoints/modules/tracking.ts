import type {
  Parcel,
  Shipment,
  ShipmentAddress,
  TrackingSetup,
} from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export type ShipmentParcelListParams = {
  investigationId: string;
  shipmentId: string;
  skip?: number;
  limit?: number;
};

export type ShipmentParams = {
  investigationId: string;
  shipmentId?: string;
};

export const SHIPMENT_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/shipment',
  name: 'shipment',
  method: 'GET',
  schema: {} as Shipment[],
  params: {} as ShipmentParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 2,
});

export type UpdateCreateShipmentParams = {
  investigationId: string;
};

export const UPDATE_SHIPMENT_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/shipment',
  name: 'shipment',
  method: 'PUT',
  schema: {} as Shipment,
  params: {} as UpdateCreateShipmentParams,
  autoRefresh: false,
});

export const CREATE_SHIPMENT_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/shipment',
  name: 'shipment',
  method: 'POST',
  schema: {} as Shipment,
  params: {} as UpdateCreateShipmentParams,
  autoRefresh: false,
});

export type InvestigationAddressParams = {
  investigationId: string;
};
export const INVESTIGATION_ADDRESSES_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/investigation/id/:investigationId/address',
  name: 'address',
  method: 'GET',
  schema: {} as ShipmentAddress[],
  params: {} as InvestigationAddressParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 2,
});

export const CREATE_INVESTIGATION_ADDRESS_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/investigation/id/:investigationId/address',
  name: 'address',
  method: 'POST',
  schema: {} as ShipmentAddress,
  params: {} as InvestigationAddressParams,
  autoRefresh: false,
});

export const UPDATE_INVESTIGATION_ADDRESS_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/investigation/id/:investigationId/address',
  name: 'address',
  method: 'PUT',
  schema: {} as ShipmentAddress,
  params: {} as InvestigationAddressParams,
  autoRefresh: false,
});

export const DELETE_INVESTIGATION_ADDRESS_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/investigation/id/:investigationId/address',
  name: 'address',
  method: 'DELETE',
  schema: {} as ShipmentAddress,
  params: {} as InvestigationAddressParams,
  autoRefresh: false,
});

export const GET_ADDRESSES_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/address',
  name: 'address',
  method: 'GET',
  schema: {} as ShipmentAddress[],
  autoRefresh: true,
  autoRefreshIntervalMinutes: 2,
});

export type ParcelListParams = {
  limit?: number;
  skip?: number;
  investigationId?: string;
  shipmentId?: string;
  parcelId?: string;
  status?: string;
  search?: string;
};
export const PARCEL_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/parcel',
  name: 'parcel',
  method: 'GET',
  schema: {} as Parcel[],
  params: {} as ParcelListParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 10,
});

export type UpdateParcelParams = {
  investigationId: string;
  shipmentId: string;
};
export const UPDATE_PARCEL_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/parcel',
  name: 'parcel',
  method: 'PUT',
  schema: {} as Parcel,
  params: {} as UpdateParcelParams,
  autoRefresh: false,
});

export const CREATE_PARCEL_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/parcel',
  name: 'parcel',
  method: 'POST',
  schema: {} as Parcel,
  params: {} as UpdateParcelParams,
  autoRefresh: false,
});

export const DELETE_PARCEL_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/parcel',
  name: 'parcel',
  method: 'DELETE',
  schema: {} as Parcel,
  params: {} as UpdateParcelParams,
  autoRefresh: false,
});

export type UpdateParcelStatusParams = {
  investigationId: string;
  parcelId: string;
  status: string;
};
export const UPDATE_PARCEL_STATUS_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/investigation/id/:investigationId/parcel/id/:parcelId/status/:status',
  name: 'parcel',
  method: 'PUT',
  schema: {} as Parcel,
  params: {} as UpdateParcelStatusParams,
  autoRefresh: false,
});

export type CreateParcelItemParams = {
  investigationId: string;
  parcelId: string;
};

export type DeleteParcelItemParams = {
  investigationId: string;
  parcelId: string;
  itemId: string;
};

export type UpdateParcelItemParams = {
  investigationId: string;
  parcelId: string;
  itemId: string;
};

export const GET_PARCEL_LABEL_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/investigation/id/:investigationId/parcel/id/:parcelId/labels',
  name: 'parcel',
  method: 'GET',
  schema: {} as Parcel,
  params: {} as {
    investigationId: string;
    parcelId: string;
  },
  autoRefresh: true,
  autoRefreshIntervalMinutes: 2,
});

export const GET_TRACKING_SETUP_ENDPOINT = IcatPlusEndpoint({
  module: 'tracking',
  path: '/setup',
  name: 'setup',
  method: 'GET',
  params: {} as {
    investigationId: string;
  },
  schema: {} as TrackingSetup,
  autoRefresh: true,
  autoRefreshIntervalMinutes: 2,
});
