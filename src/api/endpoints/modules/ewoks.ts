import type { WorkflowDescription, Job } from '../../models';
import { IcatPlusEndpoint } from '../definitions';

export const WORKFLOW_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'ewoks',
  path: '/workflows',
  name: 'workflow',
  method: 'GET',
  schema: {} as WorkflowDescription[],
  params: {},
  autoRefresh: false,
});

export const EWOKS_JOB_LAUNCH_ENDPOINT = IcatPlusEndpoint({
  module: 'ewoks',
  path: '/jobs',
  name: 'job',
  method: 'POST',
  schema: {} as Job,
  params: {} as {
    identifier?: string;
    datasetIds?: number[];
  },
  autoRefresh: false,
});

export type JobListParams = {
  investigationId?: number;
  datasetIds?: string;
  sampleIds?: string;
  jobId?: string;
  skip?: number;
  limit?: number;
  sortBy?: '_id' | 'createdAt' | 'updatedAt' | 'status';
  sortOder?: -1 | 1;
  status?: string;
  startDate?: string;
  endDate?: string;
  search?: string;
};

function computeRefreshIntervalJobs(jobs: Job[] | undefined) {
  if (
    jobs?.some((job) =>
      ['RUNNING', 'CREATED', 'SUBMITTED'].some((status) =>
        job.status.toLocaleUpperCase().includes(status),
      ),
    )
  )
    //if any of the jobs we are monitoring is running, we need to refresh often
    return 0.5;
  // otherwise we can refresh less often
  return 5;
}

export const EWOKS_JOB_LIST_ENDPOINT = IcatPlusEndpoint({
  module: 'ewoks',
  path: '/jobs',
  name: 'job',
  method: 'GET',
  schema: {} as Job[],
  params: {} as JobListParams,
  autoRefresh: true,
  autoRefreshIntervalMinutes: computeRefreshIntervalJobs,
});
