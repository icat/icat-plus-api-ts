import type { Investigation } from './investigation';
import type { ShipmentAddress } from './shipment';
import type { ContainerType } from './trackingsetup';

export type ParcelStatus = {
  createdAt: string;
  createdBy: string;
  createdByEmail: string;
  createdByFullName: string;
  comments?: string;
  id: string;
  status: string;
  updatedAt: string;
  _id: number;
};

export type ParcelContent = {
  content: ParcelContent[];
  createdAt: string;
  experimentPlan?: { key: string; value: string }[];
  processingPlan?: { key: string; value: string }[];
  id: string;
  name: string;
  sampleId?: number;
  type: string;
  updatedAt: string;
  _id: string;
  containerType?: ContainerType;
  sampleContainerPosition?: string;
  sampleChangerLocation?: number;
};

export type TrackingInformation = {
  _id: number;
  trackingNumber?: string;
  forwarder?: string;
};

export const RETURN_PARCEL_TYPE_MANAGE = 'USER_MANAGE_PAPERS';
export const RETURN_PARCEL_TYPE_ORGANIZE = 'USER_ORGANIZE_RETURN';
export const RETURN_PARCEL_TYPE = [
  RETURN_PARCEL_TYPE_MANAGE,
  RETURN_PARCEL_TYPE_ORGANIZE,
] as const;
export type ReturnParcelType = (typeof RETURN_PARCEL_TYPE)[number];

export type ReturnParcelInformation = {
  _id: number;
  returnType: ReturnParcelType;
  forwarderName?: string;
  forwarderAccount?: string;
  plannedPickupDate?: string;
};

export type Parcel = {
  comments: string;
  containsDangerousGoods: boolean;
  createdAt: string;
  description: string;
  id: string;
  investigation?: Investigation;
  investigationId: number;
  investigationName: string;
  localContactFullnames: string[];
  localContactNames: string[];
  name: string;
  returnAddress?: ShipmentAddress;
  shipmentId: string;
  shippingAddress: ShipmentAddress;
  status: string;
  statuses: ParcelStatus[];
  storageConditions: string;
  type: string;
  isReimbursed?: boolean;
  reimbursedCustomsValue?: number;
  returnInstructions?: string;
  returnParcelInformation?: ReturnParcelInformation;
  shippingTrackingInformation?: TrackingInformation;
  returnTrackingInformation?: TrackingInformation;
  updatedAt: string;
  __v: number;
  _id: number;
  content: ParcelContent[];
};
