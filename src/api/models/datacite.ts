export type NameIdentifier = {
  schemeUri: string;
  nameIdentifierScheme: string;
  nameIdentifier: string;
};

export type DataciteParticipant = {
  name: string;
  givenName?: string;
  familyName?: string;
  nameIdentifiers?: NameIdentifier[];
  contributorType?: string;
};

export type DataciteTitle = {
  title: string;
  titleType?: string;
};

export type DataciteDescription = {
  description: string;
  descriptionType: string;
};

export type DataciteSubject = {
  subject: string;
  subjectScheme: string;
};

export type DataciteDateObject = {
  date: string;
  dateType: string;
};

export type DataciteTypes = {
  resourceTypeGeneral: string;
  resourceType: string;
};

export type DataciteRelatedIdentifier = {
  relatedIdentifier: string;
  relatedIdentifierType: string;
  relationType: string;
};

export type DataciteDOIAttributes = {
  doi: string;
  url: string;
  publicationYear: string | number;
  creators?: DataciteParticipant[];
  contributors?: DataciteParticipant[];
  titles: DataciteTitle[];
  descriptions: DataciteDescription[];
  publisher: string;
  subjects: DataciteSubject[];
  dates: DataciteDateObject[];
  relatedIdentifiers?: DataciteRelatedIdentifier[];
  types: DataciteTypes;
};
