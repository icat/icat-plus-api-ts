import type { Investigation } from './investigation';
import type { Parameter } from './parameter';

export type Entity = {
  id: number;
  name: string;
  investigation: Investigation;
  parameters: Parameter[];
};
