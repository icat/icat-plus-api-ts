export type Instrument = {
  createId: string;
  createTime: string;
  description: string;
  fullName: string;
  id: number;
  modId: string;
  modTime: string;
  name: string;
  type: string;
  url: string;
};

export type InstrumentScientist = {
  id: number;
  instrument: {
    name: string;
    description: string;
  };
  user: { fullName: string; name: string };
};
