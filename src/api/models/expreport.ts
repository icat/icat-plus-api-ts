export type ExpReport = {
  proposal: string;
  nbReports: number;
  underEmbargo: boolean;
  reports: string[];
};
