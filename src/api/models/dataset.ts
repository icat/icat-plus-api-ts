import type { Entity } from './entity';
import type { Investigation } from './investigation';
import type { ListMeta } from './meta';

export const DATASET_TYPE_PROCESSED = 'processed';
export const DATASET_TYPE_REPROCESSED = 'reprocessed';
export const DATASET_TYPE_ACQUISITION = 'acquisition';

export const DATASET_TYPES = [
  DATASET_TYPE_PROCESSED,
  DATASET_TYPE_REPROCESSED,
  DATASET_TYPE_ACQUISITION,
] as const;

export type DatasetType = (typeof DATASET_TYPES)[number];

export type DatasetTimeLine = {
  startDate: string;
  endDate: string;
  investigationId: number;
  investigationName: string;
  datasetName: string;
  datasetId: number;
  sampleName: string;
  sampleId: number;
  definition: string;
  __fileCount: number;
  __volume: number;
  __elapsedTime: number;
  datasetType: DatasetType;
};

export interface Dataset extends Entity {
  startDate: string;
  endDate: string;
  location: string;
  sampleName: string;
  sampleId: number;
  type: DatasetType;
  investigation: Investigation;
  meta?: ListMeta;
  outputDatasets?: Dataset[];
  instrumentName?: string;
  techniques?: DatasetTechnique[];
}

export type DatasetParameterSearchResult = {
  name: string;
  value: string;
  units?: string;
  id: number;
  datasetId: number;
  sampleId: number;
  sampleName: string;
};

export type DatasetTechnique = {
  id: number;
  datasetId: number;
  pid: string;
  name: string;
  description?: string;
};
