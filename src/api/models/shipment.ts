export type ShipmentAddress = {
  address: string;
  city: string;
  companyName: string;
  country: string;
  createdAt: string;
  createdBy: string;
  email: string;
  investigationId: number;
  investigationName: string;
  modifiedBy: string;
  name: string;
  phoneNumber: string;
  postalCode: string;
  region: string;
  status: string;
  surname: string;
  defaultCourierCompany?: string;
  defaultCourierAccount?: string;
  updatedAt: string;
  __v: number;
  _id: number;
};

export type Shipment = {
  comments: string;
  createdAt: string;
  defaultReturnAddress?: ShipmentAddress;
  defaultShippingAddress: ShipmentAddress;
  investigationId: number;
  investigationName: string;
  name: string;
  parcels: string[];
  status: string;
  updatedAt: string;
  __v: number;
  _id: number;
};
