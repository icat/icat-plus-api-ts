import type { Dataset } from './dataset';
import type { ListMeta } from './meta';

export type DatafileResponse = {
  Datafile: Datafile;
  meta: ListMeta;
};

export type Datafile = {
  id: number;
  createId: string;
  createTime: string;
  modId: string;
  modTime: string;
  dataCollectionDatafiles: [];
  dataset: Dataset;
  destDatafiles: unknown[];
  fileSize: number;
  location: string;
  name: string;
  parameters: unknown[];
  sourceDatafiles: unknown[];
};
