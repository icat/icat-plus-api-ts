export type LogbookInvestigationStat = {
  count: number;
  instrument?: string;
  investigationId?: number;
  name?: string;
  notifications?: number;
  annotations?: number;
  startDate?: string;
  endDate?: string;
  title?: string;
  __acquisitionDatasetCount?: string;
  __acquisitionFileCount?: string;
  __acquisitionVolume?: string;
  __archivedDatasetCount?: string;
  __archivedProcessedDatasetCount?: string;
  __archivedRawDatasetCount?: string;
  __datasetCount?: string;
  __elapsedTime?: string;
  __fileCount?: string;
  __processedDatasetCount?: string;
  __processedFileCount?: string;
  __processedVolume?: string;
  __sampleCount?: string;
  __volume?: string;
};

export type LogbookInvestigationCount = {
  _id: string;
  count: number;
};
