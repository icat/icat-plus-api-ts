export type ContainerType = {
  name: string;
  capacity: number;
};

export type TrackingSetup = {
  containerTypes: ContainerType[];
  instrumentName: string;
  investigationId: string;
  investiationName: string;
  experimentPlan?: InputColumnDescription[];
  processingPlan?: InputColumnDescription[];
  sampleDescription: InputColumnDescription[];
  sampleChangerType: { name: string }[];
};

export type InputColumnDescription = {
  name: string;
  header: string;
  colored?: boolean;
  required?: boolean;
  description?: string;
} & (
  | {
      type: 'string';
      fillIncrement?: boolean;
    }
  | {
      type: 'select';
      values: SelectValueDescription[];
    }
  | {
      type: 'table';
      columns: InputColumnDescription[];
    }
  | {
      type: 'sampleFile';
      fileType: string;
    }
  | {
      type: 'sampleFileGroup';
    }
);

export type SelectValueDescription =
  | string
  | {
      value: string;
      label: string;
    };
