import type { ListMeta } from './meta';

export type WorkflowDescriptionKeywords = {
  cardinality: string;
  instrumentName: string;
  scanType: string | string[];
  definition: string;
  roles?: string[]; // roles which workflow is allowed to run. Example: ["manager", "instrumentScientist"]
  validityDays?: number; // number of days a workflow is available to run after the dataset creation time
};

export type WorkflowDescription = {
  category: string;
  label: string;
  id: string;
  input_schema?: any;
  ui_schema?: any;
  keywords: WorkflowDescriptionKeywords;
};

type InvestigationLight = {
  name: string;
  id: number;
};

/**
 * Events represents the different changes in the status, the steps and the logs for each job
 */
export type JobEvent = {
  _id: string;
  status: string;
  createdAt: string;
  updatedAt: string;
  name: string;
  type: string; // this can be status, step or log
  log: string;
};

export type Job = {
  _id: string;
  createdAt: string;
  updatedAt: string;
  datasetIds: number[];
  sampleIds: number[];
  sampleNames: string[];
  datasetNames: string[];
  investigations: InvestigationLight[];
  jobId: string;
  status: string;
  statuses: {
    status: string;
    createdAt: string;
  }[];
  steps: {
    name: string;
    createdAt: string;
    status: string;
  }[];
  identifier: string;
  userNames: string[];
  input: any;
  events: JobEvent[];
  meta: ListMeta;
};
