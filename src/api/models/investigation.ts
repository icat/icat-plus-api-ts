import type { Dataset } from './dataset';
import type { Instrument } from './instrument';
import type { ListMeta } from './meta';
import type { SimpleIcatUser } from './user';

export type Investigation = {
  createId: string;
  createTime: string;
  datasets: Dataset[];

  fileCount: number;
  fileSize: number;

  id: number;

  investigationInstruments?: {
    createId: string;
    createTime: string;
    id: number;
    instrument: Instrument;
  }[];
  instrument?: Instrument;

  keywords: string[];
  modId: string;
  modTime: string;
  name: string;
  startDate: string;
  endDate: string;
  releaseDate?: string;
  summary: string;
  title: string;
  visitId: string;
  doi?: string;

  parameters: Record<string, string>;

  meta?: ListMeta;
  canAccessDatasets?: boolean;
  investigationUsers?: { role: string; user: SimpleIcatUser }[];
};

export const PRINCIPAL_INVESTIGATOR_ROLE = 'Principal investigator';
export const LOCAL_CONTACT_ROLE = 'Local contact';
export const COLLABORATOR_ROLE = 'Collaborator';
export const INVESTIGATION_USER_ROLES = [
  PRINCIPAL_INVESTIGATOR_ROLE,
  LOCAL_CONTACT_ROLE,
  COLLABORATOR_ROLE,
] as const;
export type InvestigationUserRole = (typeof INVESTIGATION_USER_ROLES)[number];

export type InvestigationUser = SimpleIcatUser & {
  investigationId: number;
  investigationName: string;
  role: InvestigationUserRole;
  familyName: string;
  givenName: string;
};

export function isPrincipalInvestigator<T extends { name: string }>(
  icatUser: T | undefined,
  users: InvestigationUser[],
) {
  return isRole(icatUser, users, PRINCIPAL_INVESTIGATOR_ROLE);
}

export function isCollaborator<T extends { name: string }>(
  icatUser: T | undefined,
  users: InvestigationUser[],
) {
  return isRole(icatUser, users, COLLABORATOR_ROLE);
}

export function isRole<T extends { name: string }>(
  icatUser: T | undefined,
  users: InvestigationUser[],
  role: InvestigationUserRole,
) {
  if (!icatUser) return false;
  return users
    .filter((u) => u.role === role)
    .some((u) => u.name === icatUser.name);
}
