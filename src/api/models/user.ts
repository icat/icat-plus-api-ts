export type SimpleIcatUser = {
  email: string;
  fullName: string;
  id: number;
  name: string;
  orcidId?: string;
  familyName?: string;
  givenName?: string;
  affiliation?: string;
};
