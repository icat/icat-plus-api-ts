export type ResourceInformation = {
  _id: string;
  filename: string;
  fileType?: string;
  groupName?: string;
  multiplicity?: number;
  file: string;
  createdAt?: string;
};
