export type UserPreferences = {
  _id: string;
  username: string;
  selection: SelectionSetting;
  logbookFilters: LogbookSettings;
  recentlyVisited: RecentlyVisitedObject[];
  mxsettings: MXSettings;
  isGroupedBySample: boolean;
  pagination: PageSize[];
};

export type MXSettings = {
  autoProcessingRankingShell: string;
  autoProcessingRankingParameter: string;
  autoProcessingRankingDisableSpaceGroup?: boolean;
  scanTypeFilters: string[];
  cutoffs?: {
    [key in (typeof MX_SETTINGS_CUTOFF_STATS)[number]]?: {
      [key in (typeof MX_SETTINGS_CUTOFF_SHELLS)[number]]?: number;
    };
  };
};

export const MX_SETTINGS_CUTOFF_STATS = [
  'completeness',
  'resolution_limit_low',
  'resolution_limit_high',
  'r_meas_all_IPlus_IMinus',
  'mean_I_over_sigI',
  'cc_half',
  'cc_ano',
] as const;

export const MX_SETTINGS_CUTOFF_SHELLS = ['inner', 'outer', 'overall'] as const;

export type RecentlyVisitedObject = {
  url: string;
  label: string;
  target: RecentlyVisitedTarget;
};

export type RecentlyVisitedTarget = {
  instrumentName?: string;
  investigationName?: string;
  investigationStartDate?: string;
  investigationEndDate?: string;
};

export type LogbookSettings = {
  sortBy: string;
  sortOrder: number;
  types: string;
};

export type SelectionSetting = {
  datasetIds: number[];
  sampleIds: number[];
};

export type PageSize = {
  key: string;
  value: number;
};
