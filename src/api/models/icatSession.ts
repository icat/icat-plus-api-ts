export type IcatUser = {
  name: string;
  username: string;
  fullName: string;
  affiliation?: string;
  lifeTimeMinutes: number;
  isAdministrator: boolean;
  isInstrumentScientist: boolean;
  isMinter: boolean;
  sessionId: string;
  expirationTime: string;
  usersByPrefix?: UserByPrefix[];
};

export type UserByPrefix = {
  createId: string;
  createTime: string;
  dataPublicationUsers: unknown[];
  email: string;
  familyName: string;
  fullName: string;
  givenName: string;
  affiliation?: string;
  id: number;
  instrumentScientists: unknown[];
  investigationUsers: unknown[];
  modId: string;
  modTime: string;
  name: string;
  studies: unknown[];
  userGroups: unknown[];
};

export type IcatSession = {
  remainingMinutes?: number;
  userName?: string;
  user: IcatUser;
};
