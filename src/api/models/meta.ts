export type ListMeta = {
  page: {
    totalWithoutFilters: number;
    total: number;
    totalPages: number;
    currentPage: number;
  };
};
