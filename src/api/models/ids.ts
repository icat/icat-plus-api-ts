export const DATASET_STATUS_ONLINE = 'ONLINE';
export const DATASET_STATUS_RESTORING = 'RESTORING';
export const DATASET_STATUS_ARCHIVED = 'ARCHIVED';

export const DATASET_STATUS = [
  DATASET_STATUS_ONLINE,
  DATASET_STATUS_RESTORING,
  DATASET_STATUS_ARCHIVED,
] as const;

export type DatasetStatus = (typeof DATASET_STATUS)[number];

export type DatasetPaths = string[];
