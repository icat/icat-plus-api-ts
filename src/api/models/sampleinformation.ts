import type { ResourceInformation } from './resourceinformation';

export type SampleInformation = {
  _id: string;
  investigationId: number;
  sampleId: number;
  resources: ResourceInformation[];
};
