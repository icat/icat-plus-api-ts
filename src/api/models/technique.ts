export type Technique = {
  id: number;
  pid: string;
  name: string;
  description?: string;
};
