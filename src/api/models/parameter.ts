export type Parameter = {
  id: number;
  name: string;
  value: string;
  units?: string;
  description?: string;
};
