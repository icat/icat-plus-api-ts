export type LogbookEvent = {
  _id: number;
  investigationId: number;
  investigationName?: string;
  instrumentName?: string;
  datasetId?: number;
  datasetName?: string;
  type: LogbookEventType;
  tag?: LogbookTag[];
  title?: string;
  category: LogbookEventCategory;
  filepath?: string;
  filename?: string;
  fileSize?: string;
  username?: string;
  fullName?: string;
  content?: LogbookEventContent[];
  software?: string;
  machine?: string;
  source?: string;
  creationDate?: string;
  createdAt?: string;
  updatedAt?: string;
  contentType?: string;
  file?: string;
  previousVersionEvent?: LogbookEvent;
  __v?: number;
};

export const LOGBOOK_EVENT_CATEGORIES = [
  'commandline',
  'comment',
  'error',
  'debug',
  'info',
] as const;
export type LogbookEventCategory = (typeof LOGBOOK_EVENT_CATEGORIES)[number];
export const LOGBOOK_EVENT_TYPES = [
  'notification',
  'annotation',
  'broadcast',
  'attachment',
] as const;
export type LogbookEventType = (typeof LOGBOOK_EVENT_TYPES)[number];

export type LogbookTag = {
  createdAt: string;
  name: string;
  updatedAt: string;
  color?: string;
  instrumentName?: string;
  investigationId?: number;
  description?: string;
  _id: string;
  __v: number;
};

export type LogbookEventContent = {
  format: string;
  text: string;
  _id: string;
};
