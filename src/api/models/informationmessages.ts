export const INFORMATION_MESSAGE = 'INFORMATION';
export const MESSAGE_TYPES = [INFORMATION_MESSAGE] as const;
export type MessageTypes = (typeof MESSAGE_TYPES)[number];

export type InformationMessage = {
  _id: string;
  type: MessageTypes;
  message: string;
};
