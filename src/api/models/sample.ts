import type { Dataset } from './dataset';
import type { Entity } from './entity';
import type { ListMeta } from './meta';

export interface Sample extends Entity {
  createId: string;
  createTime: string;
  datasets: Dataset[];
  modId: string;
  modTime: string;
  meta: ListMeta;
}
