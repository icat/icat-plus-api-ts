export type DataCollection = {
  createId: string;
  createTime: string;
  dataCollectionDatafiles: DataCollectionDatafile[];
  dataCollectionDatasets: DataCollectionDataset[];
  dataCollectionInvestigations: unknown[];
  dataPublications: unknown[];
  doi: string;
  id: number;
  jobsAsInput: unknown[];
  jobsAsOutput: unknown[];
  modId: string;
  modTime: string;
  parameters: DataCollectionParameter[];
};

export type DataCollectionType = 'datacollection' | 'document';

export type DataCollectionDatafile = unknown;

export type DataCollectionDataset = {
  createId: string;
  createTime: string;
  dataset: {
    complete: boolean;
    createId: string;
    createTime: string;
    dataCollectionDatasets: unknown[];
    datafiles: [];
    datasetInstruments: unknown[];
    datasetTechniques: unknown[];
    endDate: string;
    fileCount: number;
    fileSize: number;
    id: number;
    location: string;
    modId: string;
    modTime: string;
    name: string;
    parameters: DataCollectionParameter[];
    startDate: string;
  };
  id: number;
  modId: string;
  modTime: string;
};

export type DataCollectionParameter = {
  name: string;
  value: string;
};
