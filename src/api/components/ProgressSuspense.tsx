import { Suspense, useState } from 'react';
import { ProgressContext } from '../utils/progress';

export function ProgressSuspense({
  children,
  render,
}: {
  children: React.ReactNode;
  render: (progress: number) => React.ReactNode;
}) {
  const [progress, setProgress] = useState(0);

  return (
    <ProgressContext.Provider value={{ progress, setProgress }}>
      <Suspense fallback={render(progress)}>{children}</Suspense>
    </ProgressContext.Provider>
  );
}
